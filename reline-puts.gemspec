# frozen_string_literal: true

require_relative 'lib/reline/puts/version'

Gem::Specification.new do |s|
	s.name = 'reline-puts'
	s.version = Reline::Puts::VERSION
	s.author = 'Alex Gittemeier'
	s.email = 'gittemeier.alex@protonmail.com'
	s.license = 'GPL-3.0'
	s.summary = 'adds Reline.puts which prints a string above the active prompt without corrupting it'
	s.homepage = 'https://gitlab.com/dot_lexg/reline-puts'

	s.files = `git ls-files`.split(/\n/)

	s.required_ruby_version = '>= 3.0' # reline itself supports >= 2.5 but I've not tested this gem on anything other than 3
	s.add_runtime_dependency 'reline', '~> 0.2'
end

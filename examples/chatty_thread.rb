# frozen_string_literal: true

require 'reline'
require 'reline/puts'

Thread.new do
	(1..).each do |i|
		sleep 1
		Reline.puts("bot#{i}: hello!")
	end
end

while (message = Reline.readline('me: ', false))
	# Not doing anything with the message since it's just an example
end

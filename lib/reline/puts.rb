#!/usr/bin/env ruby
# frozen_string_literal: true

# Prints a string above the active Reline prompt without corrupting it
# Copyright (C) 2020  Alex Gittemeier
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

require 'reline'

module Reline
	class << self
		def rerender_mutex
			@rerender_mutex ||= Mutex.new
		end

		def puts(str)
			rerender_mutex.synchronize do
				input_height = line_editor.input_height
				core.output.print "\r\e[K"
				core.output.print "\e[A\e[K" * input_height
				core.output.puts str
				core.output.print "\n" * input_height
			end
			redisplay
		end
	end

	class LineEditor
		orig_rerender = instance_method(:rerender)
		define_method(:rerender) do
			Reline.rerender_mutex.synchronize do
				orig_rerender.bind(self).()
			end
		end

		def input_height
			@first_line_started_from + @started_from
		end
	end
end
